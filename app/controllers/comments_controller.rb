class CommentsController < ApplicationController

  before_action :find_theme

  def create
    @comment = @comment.comments.create(params[:comment].permit(:content))
    @comment.email = email.text
    @comment.save

    if @comment.save
      redirect_to theme_path(@theme)
    else
      render 'new'
    end
  end

  private

  def find_theme
    @theme= Theme.find(params[:id])
  end

end
